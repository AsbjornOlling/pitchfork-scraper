{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.python
    pkgs.python310Packages.beautifulsoup4
    pkgs.python310Packages.numpy
    pkgs.python310Packages.pandas
    pkgs.python310Packages.requests
    pkgs.python310Packages.types-requests
  ];
}
