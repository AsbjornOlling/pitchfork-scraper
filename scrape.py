#!/usr/bin/env python3
import re
import itertools
from multiprocessing.dummy import Pool
from dataclasses import dataclass, asdict
import datetime
from typing import Iterable

import bs4
import requests
import numpy as np
import pandas as pd


@dataclass
class Review:
    rating: float
    reviewer: str
    genres: list[str]
    reviewed: datetime.datetime
    label: str
    release_year: int
    artist: str
    album: str


def get_review(reviewurl: str) -> float:
    r = requests.get(reviewurl)
    r.raise_for_status()
    soup = bs4.BeautifulSoup(r.text, "html.parser")

    # skip multi-reviews
    if soup.find(class_="album-picker") is not None:
        return None

    rating_elem = soup.find(class_=re.compile("^Rating-.+"))
    assert rating_elem
    rating = float(rating_elem.text)

    reviewer_elem = soup.find(class_=re.compile("^BylineLink-.+"))
    assert reviewer_elem
    reviewer = reviewer_elem.text

    info_slice_keys = soup.find_all("p", class_=re.compile("^InfoSliceKey-.+"))
    info_slices = {
        key_elem.text: list(key_elem.parent.children)[1].text
        for key_elem in info_slice_keys
    }
    genres = info_slices.get("Genre:", "").split(" / ") or None
    label = info_slices.get("Label:")
    reviewed = datetime.datetime.strptime(
        info_slices["Reviewed:"],
        "%B %d, %Y"
    )

    artist_elem = soup.find(class_=re.compile("^SplitScreenContentHeaderArtist-.+"))
    assert artist_elem
    artist = artist_elem.text

    album_elem = soup.find(class_=re.compile("^SplitScreenContentHeaderHed-.+"))
    assert album_elem
    album = album_elem.text

    release_year_elem = soup.find(class_=re.compile("^SplitScreenContentHeaderReleaseYear-.+"))
    assert release_year_elem
    release_year = int(release_year_elem.text)

    review = Review(
        rating = rating,
        reviewer = reviewer,
        genres = genres,
        reviewed = reviewed,
        label = label,
        release_year = release_year,
        artist = artist,
        album = album
    )
    print(review)
    return review


def find_reviews(nums: Iterable[int]):
    for page_num in nums:
        print(page_num)
        r = requests.get(f"https://pitchfork.com/reviews/albums/?page={page_num}")
        if r.status_code == 400:
            break

        r.raise_for_status()

        soup = bs4.BeautifulSoup(r.text, "html.parser")
        reviews = soup.find_all(class_="review__link")

        for a in reviews:
            url = f"https://pitchfork.com{a.attrs['href']}" 
            print(url)
            yield url


# get_rating("https://pitchfork.com/reviews/albums/calvin-harris-funk-wav-bounces-vol-2/")

# multi process magic here
threads = 2
def run_range(x):
    # find ratings for a range of page numbers
    # nums = itertools.count(x, threads)
    nums = list(range(x, 100, threads))

    print(nums)
    return [get_review(url) for url in find_reviews(nums)]

pool = Pool(threads)
review_batches = pool.map(run_range, range(threads))
reviews = [
    review
    for review_batch in review_batches
    for review in review_batch
    if review is not None
]

df = pd.DataFrame.from_records([asdict(review) for review in reviews])
df.to_pickle(f"{len(df)}-reviews.pickle")

breakpoint()

